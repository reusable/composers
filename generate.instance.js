var bluebird = require('bluebird')

var _ = require('lodash')
var executeOperation = require('./execute.operation')
var transformers = {
    'mustache': require('./transformers/mustache')
}

module.exports = function (entity, graph, input, instances, composer_json, app) {
    /*
        1. Get attributes directly from input where transformation is not specified
        2. Where a transformation is specified, perform the transformation and get the value
            //TODO - Handle async transformations
        3. Get values from relationships
    */
    var json = {}
    var node = composer_json.nodes[entity]
    var attribute_templates = node.values
    //console.log('attribute_templates for  ', entity, ' are ', attribute_templates)
    //Attach all the values from specified in the node by rendering templates
    Object.keys(attribute_templates).map(function (attribute) {
        var template = attribute_templates[attribute]
        if (_.isString(template)) {
            if (typeof input[template] !== 'undefined') {
                json[attribute] = input[template]
            }
        } else {
            if (template.transformer) {
                var transformer_fn = transformers[template.transformer]
                if (transformer_fn) {
                    json[attribute] = transformer_fn(template.transformation, input)
                } else {
                    console.log('Transformer ', template.transformer, ' is not defined')
                }
            }
        }
    })

    //Attach all the attributes required for relationship
    /*  
        1. Get all outgoing edges of this entity
        2. Get relationship names
        3. Add id of the target node as an attribue with relationship name + "Id"
    */

    var outEdges = graph.outEdges(entity)

    outEdges.map(function (edge) {
        var relation = graph.edge(edge)
        var to = edge.w
        json[relation + 'Id'] = instances[to].id
    })

    console.log('Created json for ', node.operation, ' of ', entity, ' is ', json)

    return executeOperation(node.operation, app.models[entity], json)

}