var mustache = require('mustache')

module.exports = function (template, input) {
    return mustache.render(template, input)
}