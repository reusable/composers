var fs = require("fs");
var createComposer = require('./create.composer')
var createEndPoint = require('./create.endpoint')
module.exports = function (app, options) {

    var composers = {}, root = options.root
    //options contain composer names and path
    options.composers.map(function (composer_name) {
        var composer_json = readJsonFile(options.path + composer_name + ".json")

        composers[composer_name] = createComposer(app, composer_json)
        createEndPoint(app, composer_name, composers[composer_name], root)
    })

    app.on('compose', function (data) {
        var composer_name = data.composer
        var input = data.input
        var totem = data.totem
        //console.log('in compose for ', composer_name, ' with ', input)

        composers[composer_name](input)
            .then(function (instances) {
                if (totem) {
                    app.emit('compose_successful_' + totem)
                }

            }).catch(function (err) {
                if (totem) {
                    app.emit('compose_failed_' + totem)
                }
                console.log('Error while composing ', composer_name, ' with ', input)
                console.log('error message - ', err)
                process.exit(0)
            })
    })
}

function readJsonFile(file_path) {

    var content = fs.readFileSync(file_path, "utf8");
    return JSON.parse(content);
}