var graphlib = require('graphlib')
var async = require('async')
var bluebird = require('bluebird')

var Graph = graphlib.Graph
var topsort = graphlib.alg.topsort
var generateInstance = require('./generate.instance')

module.exports = function (app, composer_json) {
    var graph = new Graph({ multigraph: true, directed: true })
    //add nodes and edges into the graph object
    Object.keys(composer_json.nodes).map(function (entity) {
        graph.setNode(entity, composer_json.nodes[entity])
    })

    composer_json.edges.map(function (edge) {
        graph.setEdge(edge.from, edge.to, edge.relation)
    })

    var order = graphlib.alg.topsort(graph).reverse()

    //console.log('Order for composer ', composer_json.title, ' ', order)

    return function (input) {
        var D = bluebird.defer()
        var instances = {}

        async.mapSeries(order,
            function (entity, callback) {
                var operation = composer_json.nodes[entity].operation
                generateInstance(entity, graph, input, instances, composer_json, app)
                    .then(function (instance) {
                        //console.log('After ' + operation + ' instance for ', entity, ' is ', instance)
                        if (instance) {
                            instances[entity] = instance
                            callback(null, instance)
                        } else {
                            callback({ message: 'Could not generate instance for ' + entity, input: input })
                        }

                    }).catch(function (err) {
                        console.log('ERR generating instance for ', entity, ' is ', err)
                        process.exit(0)
                        callback(err)
                    })
            }, function (error, output) {
                //final callback
                if (error) {
                    D.reject(error)
                } else {
                    D.resolve(instances)
                }
            })

        return D.promise
    }
}