var bluebird = require('bluebird')
module.exports = function (operation, model, json) {

    switch (operation) {
        case "upsert":
            return model.upsert(json)
        case "findOne":
            return model.findOne({ where: json })
        default:
            return bluebird.reject('Operation ' + operation + " is not yet Implemented")
    }
}