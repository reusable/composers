var _ = require('lodash')

module.exports = function (app, composer_name, composer_fn, root) {
    var model = app.models[root]
    var fn_name = _.camelCase(composer_name)

    console.log('Adding remote method ', fn_name, ' on ', root)
    model[fn_name] = function (input, callback) {
        composer_fn(input)
            .then(function (instances) {
                callback(null, instances)
            }).catch(function (err) {
                callback(err)
            })
    }

    model.remoteMethod(fn_name, {
        accepts: [{
            arg: 'input',
            type: 'object'
        }],
        returns: {
            arg: 'response',
            type: 'object'
        },
        http: {
            path: '/' + fn_name,
            verb: 'post'
        }
    })
}